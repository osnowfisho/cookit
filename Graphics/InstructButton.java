import processing.core.PApplet;
import processing.core.*;

class InstructButton {

  private PApplet sketch;
  private String name;
  private int xPos, yPos;
  private int buttonWidth = 140;
  private int buttonHeight = 45;
  private int buttonColor = 10;
  private int buttonHighLight = 51;
  private boolean mouseOverbutton = false;

  String getName() {
    return name;
  }

  boolean getMouseButtonOver() {
    return mouseOverbutton;
  }

  InstructButton(PApplet sketch, String name, int xPos, int yPos) {
    this.sketch = sketch;
    this.name = name;
    this.xPos = xPos;
    this.yPos = yPos;
  }

  public void drawButton() {
    sketch.noStroke();
    sketch.fill(0,153,255);
    sketch.rect(xPos, yPos, buttonWidth, buttonHeight, 10);

    sketch.textSize(25);
    sketch.fill(255);
    sketch.text(name, xPos+20, yPos+30); // change the text in the button
  }

  // Detect if mouse is over the rectangl35
  public void mouseOverButton()  {

    if (sketch.mouseX >= xPos && sketch.mouseX <= xPos+buttonWidth &&
        sketch.mouseY >= yPos && sketch.mouseY <= yPos+buttonHeight) {
      mouseOverbutton = true;
    }
    else {
      mouseOverbutton = false;
    }
  }

  public void drawButtonHL() {
    sketch.noStroke();
    sketch.fill(255);
    sketch.rect(xPos-5, yPos-5, buttonWidth+10, buttonHeight+10, 10);


    sketch.noStroke();
    sketch.fill(0,153,200);
    sketch.rect(xPos, yPos, buttonWidth, buttonHeight, 10);

    sketch.textSize(30);
    sketch.fill(255);
    sketch.text(name, xPos+15, yPos+30);
  }

}

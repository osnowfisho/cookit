import processing.core.PApplet;
import processing.core.*;

class Whisk {
	private PApplet sketch;
	PImage a, b, c;
	int y;
	int x;
	int n = 0;
	private int xPos, yPos;
	private boolean animating;
	private boolean direction = true;

	Whisk (PApplet sketch, int xPos, int yPos) {
		this.sketch = sketch;
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public void whiskSetup() {
		a = sketch.loadImage("images/whisk.png");
		b = sketch.loadImage("images/bowl.png");
		c = sketch.loadImage("images/bowlfront.png");
		x=xPos;
		y=yPos;
	}

	public void anime() {

		if(n >= 80){
			animating = false;
			return;
		}

		sketch.image(b, xPos-10, yPos+10, 150, 150);
		sketch.image(a, x, y, 105, 105);
		sketch.image(c, xPos-10, yPos+10, 150, 150);
		

		if (n<80) {
			if (direction) {
				x=x+10;
				if (x >= xPos+80) {
					direction = false;
				}
			}
			else {
				x=x-10;
				if (x <= xPos) {
					direction = true;
				}
			}
			n++;
		}
	}

	public void set_animating() {
		animating = true;
	}

	public boolean animating() {
		return animating;
	}
}

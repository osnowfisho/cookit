package Logic;

public class Instruction{
  private String instr;

  public Instruction(String new_instr) {
    instr = new_instr;
  }

  public Instruction() {

  }

  public String get_instr() {
	   return instr;
  }

  public void set_instr(String new_instr) {
	   instr = new_instr;
  }

  public boolean is_instr(String new_instr) {
	if(new_instr.equals("chop")){
	  return true;
    }
    return false;
  }

  public static void main(String[] args){
    Instruction i = new Instruction();
    assert(i.is_instr("chop"));
    i.set_instr("chop");
    assert(i.get_instr().equals("chop"));
    System.out.println("Tests pass");
  }
}

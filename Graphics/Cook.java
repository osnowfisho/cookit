import Logic.*;
import java.io.*;
import java.util.*;

public class Cook {
  private DisplayController view;
  private Ingredient current_ingr;
  private Gamelogic gl;
  private Instruction current_instr;
  private ArrayList<String> actions = new ArrayList<String>();

  public static void main(String[] args){
    Cook a = new Cook();
  }


  Cook() {
	  current_instr = new Instruction();
  }

  Cook(DisplayController dc) {
	  current_instr = new Instruction();
    view = dc;
    gl = new Gamelogic();
  }
  
  void setup() {
    gl.add_ingr("onion");
    gl.add_ingr("eggs");
    gl.add_ingr("tomatoes");
    gl.add_ingr("garlic");
  }

  void act(String currentInstruction, String currentIngredient){
  	try{ 
      gl.act(currentInstruction, currentIngredient);
      if(!currentInstruction.equals("chop")){
        actions.add(currentInstruction + "ed " + currentIngredient);
      }
      else {
        actions.add("chopped " + currentIngredient);
      }
    }
    catch(Error e) {
      view.setError(e.getMessage());
      return;
    }
    if(currentInstruction.equals("chop")) {
      view.setknifeTest();
      if (currentIngredient.equals("tomatoes")) {
        view.choppedtomatoes = true;
      }
      if (currentIngredient.equals("onion")) {
        view.choppedonion = true;
      }
      if (currentIngredient.equals("garlic")) {
        view.choppedgarlic = true;
      }
    }
    if(currentInstruction.equals("cook")) {
      view.setSpatulaTest();
      if (currentIngredient.equals("eggs")) {
        view.cookedeggs = true;
      }
      if (currentIngredient.equals("onion")) {
        view.cookedonion = true;
      }
      if (currentIngredient.equals("garlic")) {
        view.cookedgarlic = true;
      }
      if (currentIngredient.equals("tomatoes")) {
        view.cookedtomatoes = true;
      }
    }
    if (currentInstruction.equals("whisk")) {
      view.setWhiskTest();
      view.whiskedeggs = true;
    }
  }

  String ingr_name() {
    return current_ingr.get_name();
  }

  String get_instr() {
    return current_instr.get_instr();
  }
  
  int num_actions() {
    return actions.size();
  }
  
  String get_action(int i) {
    return actions.get(i);
  }


}

import processing.core.PApplet;
import processing.core.*;

class Spatula {
	private PApplet sketch;
	PImage a;
	int y;
	int x;
	int n = 0;
	private int ingr_width;
	private int xPos, yPos;
	private boolean animating;
	private boolean direction = true;

	Spatula (PApplet sketch, int xPos, int yPos) {
		this.sketch = sketch;
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public void spatulaSetup() {
		a = sketch.loadImage("images/spatula.png");
		x=xPos;
		y=yPos;
	}

	// public void set_width(int width) {
	// 	ingr_width = width;
	// }

	public void anime() {
    animating = true;
		if(n >= 80){
			animating = false;
      n = 0;
			return;
		}

		sketch.image(a, x, y, 200, 200);

		if (n<80) {
			if (direction) {
				y=y+10;
				if (y >= yPos+100) {
					direction = false;
				}
			}
			else {
				y=y-10;
				if (y <= yPos) {
					direction = true;
				}
			}
			n++;
		}
	}

	public void set_animating() {
		animating = true;
	}

	public boolean animating() {
		return animating;
	}
}

import processing.core.PApplet;

class Board {
	private PApplet sketch;

	private int xPos, yPos, rectW, rectH;
	private int light, dark;

	Board(PApplet sketch, int xPos, int yPos, int rectW, int rectH, int light, int dark) {
		this.sketch = sketch;
		this.xPos = xPos;
		this.yPos = yPos;
		this.rectW = rectW;
		this.rectH = rectH;
		this.light = light;
		this.dark = dark;
	}

	public void drawBoard() {
		sketch.noStroke();
		sketch.fill(light);
		sketch.rect(xPos, yPos, rectW, rectH, 20);
		sketch.fill(dark);
		sketch.rect(xPos+10, yPos+10, rectW-20, rectH-20, 30);
		sketch.fill(light);
		sketch.rect(xPos+22, yPos+22, rectW-44, rectH-44, 30);
	}
}

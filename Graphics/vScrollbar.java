import processing.core.*;

class VScrollbar {
  private PApplet sketch;

  private int swidth, sheight;    // width and height of bar
  private int xpos, ypos;         // x and y position of bar
  private float spos, newspos;    // x position of slider
  private int sposMin, sposMax;   // max and min values of slider
  private int loose;              // how loose/heavy
  private boolean over;           // is the mouse over the slider?
  private boolean locked;
  private float ratio;

  VScrollbar (PApplet sk, int xp, int yp, int sw, int sh, int l) {
    sketch = sk;
    swidth = sw;
    sheight = sh;
    int heighttowidth = sh - sw;
    ratio = (float)sh / (float)heighttowidth;
    xpos = xp-swidth/2;
    ypos = yp;
    spos = ypos + sheight/2 - swidth/2;
    newspos = spos;
    sposMin = ypos;
    sposMax = ypos + sheight - swidth;
    loose = l;
  }

  void update() {
    if(over()) {
      over = true;
    } else {
      over = false;
    }
    if(sketch.mousePressed && over) {
      locked = true;
    }
    if(!sketch.mousePressed) {
      locked = false;
    }
    if(locked) {
      newspos = sketch.constrain(sketch.mouseY-swidth/2, sposMin, sposMax);
    }
    if(sketch.abs(newspos - spos) > 1) {
      spos = spos + (newspos-spos)/loose;
    }
  }

  int constrain(int val, int minv, int maxv) {
    return sketch.min(sketch.max(val, minv), maxv);
  }

  boolean over() {
    if(sketch.mouseX > xpos && sketch.mouseX < xpos+swidth &&
    sketch.mouseY > ypos && sketch.mouseY < ypos+sheight) {
      return true;
    } else {
      return false;
    }
  }

  void display() {
    sketch.fill(255);
    sketch.rect(xpos, ypos, swidth, sheight);
    if(over || locked) {
      sketch.fill(255, 255, 0);
    } else {
      sketch.fill(255, 0, 0);
    }
    sketch.rect(xpos, spos, swidth, swidth);
  }

  float getPos() {
    // Convert spos to be values between
    // 0 and the total width of the scrollbar
    return spos * ratio;
  }
}

The Cookit game, made by Group H(appy Tree Friends).

To compile, run: `javac -cp core.jar *.java ./Logic/*.java`

To run:
`java -cp .:core.jar DisplayController` for MacOS, and
`java -cp .;core.jar DisplayController` for Windows

Program structure:

`Board.java`: draws the cutting board.

`Pan.java`: draws a pan.

`ErrorDisplay.java`: draws the error message board.

`IngredientDisplay.java`: draws all ingredients.

`InstructButton.java`: draws all instruction buttons.

`Knife.java`: draws a knife with animation.

`Spatula.java`: draws a spatula with animation.

`Whisk.java`: draws a bowl and whisk with animation.

`Gamelogic.java`: game logic.

`Cook.java`: main controller.

`DisplayController.java`: main display.

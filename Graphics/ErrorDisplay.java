import processing.core.PApplet;

class ErrorDisplay {
  private PApplet sketch;
  private int xPos = 500, yPos = 300;
  private int eWidth = 520, eHeight = 220;
  private boolean mouseoverX = false;

  // Constructor
  ErrorDisplay(PApplet sketch) {
    this.sketch = sketch;
  }

  // Draw error message
  public void drawErrorMsg(String errorMsg) {
    if (errorMsg.equals("")) {return;}

    sketch.noStroke();
    sketch.fill(0,153,255); // background color
    sketch.rect(xPos, yPos, eWidth, eHeight);

    sketch.textSize(30);
    sketch.fill(255, 255, 255); // text color
    sketch.text(errorMsg, xPos+15, yPos+50, 500, 200);
    sketch.text("X", 980, 330);
  }

  public void update_mouse() {
    if(sketch.mouseX >= 980 && sketch.mouseY <= 1000 && sketch.mouseY >= yPos && sketch.mouseY <=330) {
      mouseoverX = true;
      return;
    }
    mouseoverX = false;
  }

  public boolean getmouseoverX() {
    return mouseoverX;
  }


}

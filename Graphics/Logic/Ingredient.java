package Logic;

public class Ingredient {
	private boolean chopped;
	private boolean cooked;
    private boolean whisked;
	private String name;

	public static void main(String[] args){
		Ingredient onion = new Ingredient("Onion");
		assert(!onion.is_chopped());
		onion.chop();
		assert(onion.is_chopped());
		assert(onion.get_name().equals("Onion"));
		System.out.println("tests pass");
	}

	public Ingredient(String name) {
		this.name = name;
	}

	public void set_name(String name) {
	  this.name = name;
	}

	public void do_instr(Instruction i) {
	  if(i.get_instr().equals("chop")){
	    chop();
	  }
	}

	public void chop() {
		chopped = true;
	}

  public void whisk() {
    whisked = true;
  }

  public void cook() {
    cooked = true;
  }

  public boolean is_cooked() {
    return cooked;
  }

  public boolean is_whisked() {
    return whisked;
  }

	public boolean is_chopped() {
		return chopped;
	}

	public String get_name() {
		return name;
	}
}

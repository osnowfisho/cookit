package Logic;

import java.util.*;

public class Gamelogic {

  private ArrayList<Ingredient> ingredients;

  public Gamelogic() {
    ingredients = new ArrayList<Ingredient>();
  }

  Gamelogic(ArrayList<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }

  public static void main(String[] args){
    Gamelogic gl = new Gamelogic();
    gl.run();
  }

  void run() {
    add_ingr("onion");
    add_ingr("eggs");
    add_ingr("garlic");
    add_ingr("tomatoes");
    act("chop", "onion");
    act("whisk", "eggs");
    assert(get_ingredient("onion").is_chopped());
    act("chop", "garlic");
    act("chop", "tomatoes");
    act("cook", "eggs");
    act("cook", "onion");
    act("cook", "tomatoes");
  }


  Ingredient get_ingredient(String name) {
    for(Ingredient i: ingredients){
      if(i.get_name().equals(name)){
        return i;
      }
    }
    throw new Error("Must add an ingredient");
  }

  public void act(String instr, String ingr) {
    Ingredient i = get_ingredient(ingr);
    if(instr.equals("chop")){
      chop_action(i);
      return;
    }
    if(instr.equals("whisk")){
      whisk_action(i);
      return;
    }
    if(instr.equals("cook")){
      cook_action(i);
      return;
    }
    throw new Error("Must add an instruction");
  }
  
  void cook_action(Ingredient i){
    String name = i.get_name();
    for(Ingredient ing: ingredients){
      if(!ing.is_chopped()){
        if(!ing.is_whisked()){
          throw new Error("It is best practice to have all ingredients ready before you start cooking");
        }
      }
    }
    if(i.is_cooked()){
      throw new Error(name + " has already been cooked.");
    }
    if(!(i.is_whisked() || i.is_chopped())){
      throw new Error(name + " not in the correct state to be cooked.");
    }
    if(!name.equals("eggs")){
      if(!get_ingredient("eggs").is_cooked()){
        throw new Error("Eggs should be cooked before the other ingredients");
      }
    }
    if(name.equals("tomatoes")){
      if(!(get_ingredient("onion").is_cooked() && get_ingredient("garlic").is_cooked())){
        throw new Error("Other ingredients must be cooked before the tomatoes");
      }
    }
    i.cook();
  }
  
  void chop_action(Ingredient i){
    String name = i.get_name();
    if(i.is_chopped()){
      throw new Error(name + " has already been chopped.");
    }
    if(name.equals("eggs")){
      throw new Error("Eggs should not be chopped");
    }
    i.chop();
  }
  
  void whisk_action(Ingredient i){
    String name = i.get_name();
    if(!name.equals("eggs")){
      throw new Error(name + " cannot be whisked.");
    }
    if(i.is_whisked()) {
      throw new Error("Eggs have already been whisked");
    }
    i.whisk();
    return;
  }

  public void add_ingr(String name) {
    Ingredient i = new Ingredient(name);
    ingredients.add(i);
  }
  
}

import processing.core.*;
import java.util.*;
import processing.event.MouseEvent;

public class DisplayController extends PApplet {

  private String currentIngredient = "";
  private String currentInstruction = "";

  int baseColor;
  int currentColor;
  PImage a;
  PImage b;
  PImage background;
  PImage current;
  PImage new_image;
  PImage start_bg;
  PImage end_bg;
  int i=0;
  boolean cursor1 = false;
  boolean cursor2 = false;
  PFont font;
  Cook c = new Cook(this);
  Board board = new Board(this, 450, 400, 360, 260, 0xffC19A6B, 0xff937551);
  Pan pan = new Pan(this, 1050, 550, 320, 320, 50);
  Knife knife = new Knife(this, 570, 450);
  Spatula spatula = new Spatula(this, 980, 450);
  Whisk whisk = new Whisk(this, 550, 450);
  boolean choppedtomatoes = false;
  boolean choppedonion = false;
  boolean choppedgarlic = false;
  boolean cookedtomatoes = false;
  boolean cookedonion = false;
  boolean cookedgarlic = false;
  boolean cookedeggs = false;
  boolean whiskedeggs = false;
  private List<InstructButton> instructs = new ArrayList<InstructButton>();
  private List<IngredientDisplay> ingredients = new ArrayList<IngredientDisplay>();
  private InstructButton execute = new InstructButton(this, "Execute", 40, 300);
  private InstructButton start = new InstructButton(this, "Start", 580, 570);
  private ErrorDisplay error = new ErrorDisplay(this);
  private String errorMsg = "";
  private String board_ingr = "";

  private boolean animation = false;
  private boolean knifeTest;
  private boolean spatulaTest;
  private boolean spatulaAnimation = false;
  private boolean whiskTest;
  private boolean whiskAnimation = false;

  private int gameState;
  final int gameStart = 0;
  final int gameRun = 1;
  final int gameFinished = 2;

  private Audio music = new Audio("test1.wav");
  private Audio soundEffect = new Audio("chop.wav");
  private Audio whiskSound = new Audio("whisk.wav");
  private Audio cookSound = new Audio("cook.wav");

  public void setup() {
    frameRate(60);
    setupInstructs();
    setupIngredients();
    baseColor = color(102);
    currentColor = baseColor;
    ellipseMode(CENTER);
    c.setup();
    a = loadImage("123.png");
    b = loadImage("456.png");
    background = loadImage("images/background.jpg");
    start_bg = loadImage("images/start11.png");
    end_bg = loadImage("images/end.png");

    current = a;
    new_image = a;
    // setupBackground();
    font = createFont("Chalkduster.ttf",10);
    textFont(font);
    // animations setup
    knife.knifeSetup();
    spatula.spatulaSetup();
    whisk.whiskSetup();
	  gameState = gameStart;
	  music.play();
  }

  void setupInstructs() {
    InstructButton cookButton = new InstructButton(this, "cook", 40, 110);
    InstructButton chopButton = new InstructButton(this, "chop", 40, 170);
    InstructButton whiskButton = new InstructButton(this, "whisk", 200, 170);
    instructs.add(chopButton);
    instructs.add(whiskButton);
    instructs.add(cookButton);
  }

  void setupIngredients() {
    IngredientDisplay onion = new IngredientDisplay(this, "onion", 450, 50, 150, 150);
    IngredientDisplay tomatoes = new IngredientDisplay(this, "tomatoes", 600, 50, 150, 150);
    IngredientDisplay garlic = new IngredientDisplay(this, "garlic", 750, 50, 150, 150);
    IngredientDisplay eggs = new IngredientDisplay(this, "eggs", 900, 50, 120, 150);

    ingredients.add(onion);
    ingredients.add(tomatoes);
    ingredients.add(garlic);
    ingredients.add(eggs);
  }

  void drawInstructs() {
    cursor1 = false;
    for (InstructButton i: instructs) {
      if (i.getMouseButtonOver()) {
        i.drawButtonHL();
        cursor1 = true;
      }
      else {
        i.drawButton();

      }
    }
  }

  void drawIngredients() {
    cursor2 = false;
    for (IngredientDisplay i: ingredients) {
      if (i.getMouseButtonOver()) {
        i.drawIngredientHL();
        cursor2 = true;
      }
      else {
        i.drawIngredient();

      }
    }
  }

  // Constantly looping.
  public void draw() {
  	updateMousePosition(mouseX, mouseY);
  	if (gameState == gameStart) {
		image(start_bg, 0, 0);
		textSize(60);
  		fill(0);
  		text("CookIt!", 500, 380);
        textSize(20);
        text("Welcome to cookit, where you work out how to cook different recipes. Today it's scrambled eggs with tomatoes!", 460, 410, 400, 200);

  		if (start.getMouseButtonOver()) {
  			start.drawButtonHL();
  			cursor(HAND);
  		}
  		else {
  			start.drawButton();
  			cursor(ARROW);
  		}
  	}
  	if (gameState == gameRun) {
  		load_images();
    	setupBackground();
    	draw_board();
    	knifeTest(); // knife test
    	setanimation();
    	spatulaTest();
    	setSpatulaAnimation();
    	whiskTest();
    	setWhiskAnimation();
    	draw_current_instr();
    	draw_current_ingr();
    	cursorChange();
    	print_actions();
    	drawErrorMsg();
    	soundEffects();
        game_over();
  	}
    if (gameState == gameFinished) {
      image(end_bg, 0, 0);
      textSize(35);
      fill(0,0,0);
      text("Congratulations! You've made scrambled eggs with tomatoes", 330, 50, 700, 700);
    }
  }

  // Check if mouse is within any shapes and update the booleans.
  public void updateMousePosition(int x, int y) {
    for (InstructButton i: instructs) {
      i.mouseOverButton();
    }
    for (IngredientDisplay i: ingredients) {
      i.mouseOverButton();
    }
    execute.mouseOverButton();
    start.mouseOverButton();
    error.update_mouse();
  }

  public void setupBackground() {
    // background(0xffFFFCCC);
    // noStroke(); // Disables drawing the stroke (outline)
    // fill(0xffFFB266);// Sets the color used to fill shapes.
    // rect(0, 0, width/4, 120); // "width" is a system variable that stores the width of the display window.
    // noStroke();
    // fill(0xffFF9999);
    // rect(0, 120, width/4, height-120);
    image(background, 0, 0);

    board.drawBoard();
    pan.drawPan();

    drawInstructs();
    drawIngredients();

    drawExecuteButton();
  }

  public void drawExecuteButton() {
    if (execute.getMouseButtonOver()) {
      execute.drawButtonHL();
    }
    else {
    	execute.drawButton();
    }
  }

  public void cursorChange() {
    if (execute.getMouseButtonOver() || cursor1 || cursor2) {
      cursor(HAND);
    }
    else {
      cursor(ARROW);
    }
  }

  // old scrollbar
  // public void mouseWheel(MouseEvent event) {
  //   // scrollbar.updateScroll(event.getCount() * 3);
  //   // contrain(event.getCount() * 3,)
  // }

  public void mousePressed() {
    // when a user click the execute button
    if(animation || spatulaAnimation || whiskAnimation){
      return;
    }
    if(!errorMsg.equals("")){
      exitError();
      return;
    }
    if (start.getMouseButtonOver()) {
    	gameState = gameRun;
    }
    if (execute.getMouseButtonOver()) {
      c.act(currentInstruction, currentIngredient);
      currentInstruction = "";
      currentIngredient = "";
    }
    // check if any instruction gets clicked
    for (InstructButton i: instructs) {
      if (i.getMouseButtonOver()) {
        currentInstruction = i.getName();
      }
    }
    // check if any ingredient gets clicked
    for (IngredientDisplay i: ingredients) {
      if (i.getMouseButtonOver()) {
        currentIngredient = i.getName();
        board_ingr = i.getName();
      }
    }
    exitError();
  }

  public void change_image() {
	  if (i == 0) {
        new_image = b;
        i = 1; // To enable switching images.
      }
      else {
        new_image = a;
        i = 0;
      }
  }

  public void print_actions() {
    textSize(15);
    for(i = 1; i <= c.num_actions(); i++) {
      text(i + ": " + c.get_action(i-1), 30, 370 + (i * 30));
    }
  }

  public void load_images() {
    if(animation == false) {
      current = new_image;
    }
  }

  public void knifeTest() {
    if (knifeTest) {
      knife.set_width(80);
      knife.anime();
    }
  }

  public void setanimation() {
    if(knife.animating()) {
      animation = true;
      return;
    }
    animation = false;
    knifeTest = false;
  }

  public void setknifeTest() {
    knifeTest = true;
    animation = true;
  }

  public void spatulaTest() {
    if (spatulaTest) {
      spatula.anime();
    }
  }

  public void setSpatulaAnimation() {
    if(spatula.animating()) {
      spatulaAnimation = true;
      return;
    }
    spatulaAnimation = false;
    spatulaTest = false;
  }

  public void setSpatulaTest() {
    spatulaTest = true;
    spatulaAnimation = true;
  }

  public void whiskTest() {
    if (whiskTest) {
      whisk.set_animating();
      whisk.anime();
    }
  }

  public void setWhiskAnimation() {
    if(whisk.animating()) {
      whiskAnimation = true;
      return;
    }
    whiskAnimation = false;
  }

  public void setWhiskTest() {
    whiskTest = true;
    whiskAnimation = true;
  }

  public void setError(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  public void draw_current_instr() {
    textSize(15);
    text("Current instruction: " + currentInstruction, 30, 250);
  }

  public void draw_current_ingr() {
    textSize(15);
    text("Current ingredient: " + currentIngredient, 30, 280);
  }

  public void exitError() {
    if(error.getmouseoverX()){
      errorMsg = "";
    }
  }

  void drawErrorMsg() {
    if(errorMsg.equals("")){
      return;
    }
    error.drawErrorMsg(errorMsg);
    board_ingr = "";
  }

  IngredientDisplay get_ingr_display(String ingr) {
    for(IngredientDisplay id: ingredients) {
      if(id.getName().equals(ingr)){
        return id;
      }
    }
    throw new Error("Could not find ingredient");
  }

  boolean game_over() {
    if(spatulaAnimation == false && c.num_actions() == 8){
      gameState = gameFinished;
      return true;
    }
    return false;
  }

  void draw_board() {
    if(board_ingr.equals("")){
      return;
    }
    IngredientDisplay ingr = get_ingr_display(board_ingr);
    if (!animation) {
    	if (board_ingr.equals("tomatoes")) {
    		if (!choppedtomatoes) {
    			ingr.set_x_y(550, 450).drawIngredient();
    		}
    		else {
    			if (!cookedtomatoes) {
    				ingr.set_x_y(550, 450).drawChopped(board_ingr);
            if (cookedeggs) {
              ingr.set_x_y(980, 480).drawCooked("cookedeggs");
            }
    				if (cookedeggs && cookedgarlic && !cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_garlic");
    				}
    				if (cookedeggs && !cookedgarlic && cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion");
    				}
    				if (cookedeggs && cookedgarlic && cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion_garlic");
    				}
    			}
    			else {
    				ingr.set_x_y(980, 480).drawCooked("all");
    			}
    		}
    	}
    	if (board_ingr.equals("onion")) {
    		if (!choppedonion) {
    			ingr.set_x_y(550, 450).drawIngredient();
    		}
    		else {
    			if (!cookedonion) {
    				ingr.set_x_y(550, 450).drawChopped(board_ingr);
    				if (cookedeggs && !cookedgarlic) {
    					ingr.set_x_y(980, 480).drawCooked("cookedeggs");
    				}
    				if (cookedeggs && cookedgarlic) {
    					ingr.set_x_y(980, 480).drawCooked("egg_garlic");
    				}
    			}
    			else {
    				if (cookedeggs && !cookedgarlic) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion");
    				}
    				else if (cookedeggs && cookedgarlic) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion_garlic");
    				}
    				else {
    					ingr.set_x_y(980, 450).drawChopped(board_ingr);
    				}
    			}
    		}
    	}
    	if (board_ingr.equals("garlic")) {
    		if (!choppedgarlic) {
    			ingr.set_x_y(550, 450).drawIngredient();
    		}
    		else {
    			if (!cookedgarlic) {
    				ingr.set_x_y(550, 450).drawChopped(board_ingr);
    				if (cookedeggs && !cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("cookedeggs");
    				}
    				if (cookedeggs && cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion");
    				}
    			}
    			else {
    				if (cookedeggs && !cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_garlic");
    				}
    				else if (cookedeggs && cookedonion) {
    					ingr.set_x_y(980, 480).drawCooked("egg_onion_garlic");
    				}
    				else {
    					ingr.set_x_y(980, 480).drawChopped(board_ingr);
    				}
    			}
    		}
    	}
    	if (board_ingr.equals("eggs")) {
        if (whiskedeggs && !whiskAnimation && !spatulaAnimation && !cookedeggs) {
          ingr.set_x_y(550, 450).drawWhiskedEggs();
        }
    		if (cookedeggs && !cookedonion && !cookedgarlic) {
    			ingr.set_x_y(980, 480).drawCooked("cookedeggs");
    		}
    		if (cookedeggs && !cookedonion && cookedgarlic) {
    			ingr.set_x_y(980, 480).drawCooked("egg_garlic");
    		}
    		if (cookedeggs && cookedonion && !cookedgarlic) {
    			ingr.set_x_y(980, 480).drawCooked("egg_onion");
    		}
    		if (cookedeggs && cookedonion && cookedgarlic) {
    			ingr.set_x_y(980, 480).drawCooked("egg_onion_garlic");
    		}
    	}
    }
    else {
    	ingr.set_x_y(550,450).drawIngredient();
    }
  }

  void soundEffects() {
  	if (animation) {
  		soundEffect.play();
  	}
    else {
      soundEffect.stop();
    }
    if (whiskAnimation) {
      whiskSound.play();
    }
  	else {
      whiskSound.stop();
    }
    if (spatulaAnimation) {
    	cookSound.play();
    }
    if (game_over()) {
    	cookSound.stop();
    }
  }

  public void settings() {  size(1280, 720); }

  static public void main(String[] passedArgs) {

    String[] appletArgs = new String[] { "DisplayController" };

    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }

  }

}

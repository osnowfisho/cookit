import processing.core.PApplet;

class Pan {
	private PApplet sketch;

	private int xPos, yPos, panW, panH;
	private int color;

	Pan (PApplet sketch, int xPos, int yPos, int panW, int panH, int color) {
		this.sketch = sketch;
		this.xPos = xPos;
		this.yPos = yPos;
		this.panW = panW;
		this.panH = panH;
		this.color = color;
	}

	public void drawPan() {
		sketch.noStroke();
		sketch.fill(color);
		sketch.ellipse(xPos, yPos, panW, panH);

		sketch.rect(xPos-10, yPos+100, 20, 100);
		sketch.rect(xPos-20, yPos+190, 40, 100, 10);

		sketch.fill(20);
		sketch.ellipse(xPos, yPos, panW-15, panH-15);
		sketch.fill(color);
		sketch.ellipse(xPos, yPos, panW-50, panH-50);
	}
}

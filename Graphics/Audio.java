import java.io.*;
import javax.sound.sampled.*;

public class Audio {
	private Clip clip;

	public Audio(String musicLocation) {
		try {
			File musicPath = new File(musicLocation);
			AudioInputStream audioImput = AudioSystem.getAudioInputStream(musicPath);
			clip = AudioSystem.getClip();
			clip.open(audioImput);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	// void playMusic(String musicLocation) {
	// 	try {
	// 		File musicPath = new File(musicLocation);
	// 		AudioInputStream audioImput = AudioSystem.getAudioInputStream(musicPath);
	// 		Clip clip = AudioSystem.getClip();
	// 		clip.open(audioImput);
	// 		clip.start();
	// 		clip.loop(Clip.LOOP_CONTINUOUSLY);
	// 	}
	// 	catch(Exception e) {
	// 		e.printStackTrace();
	// 	}
	// }

	void play() {
		clip.start();
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	void stop() {
		if(clip.isRunning()) {
			clip.stop();
		}
	}
	
	// void close() {
	// 	stop();
	// 	clip.close();
	// }
}
import processing.core.PApplet;
import processing.core.*;

class Knife {
	private PApplet sketch;
	PImage a;
	int y;
	int x;
  private int ingr_width;
	private int xPos, yPos;
  private boolean animating;

	Knife (PApplet sketch, int xPos, int yPos) {
		this.sketch = sketch;
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public void knifeSetup() {
		a = sketch.loadImage("images/knife.png");
		x=xPos;
		y=yPos;
	}
  
  public void set_width(int width) {
    ingr_width = width;
  }

	public void anime() {
    animating = true;
    if(x >= xPos+ingr_width && y < yPos+100){
      animating = false;
      x = xPos;
      y = yPos;
      return;
    }

		sketch.image(a, x, y, 150, 100);
		y=y+10;

		if (x<xPos+ingr_width) {
			if (y>yPos+80) {
				y=yPos;
				x=x+20;
			}
		}
	}
  
  public boolean animating() {
    return animating;
  }
}

import processing.core.PApplet;
import processing.core.*;

class IngredientDisplay {

  private PApplet sketch;
  private String name;
  private int xPos, yPos;
  private int xSize, ySize;
  private PImage img;
  private boolean mouseOverbutton;

  IngredientDisplay(PApplet sketch, String name, int xPos, int yPos, int xSize, int ySize) {
    this.sketch = sketch;
    this.name = name;
    this.xPos = xPos;
    this.yPos = yPos;
    this.xSize = xSize;
    this.ySize = ySize;
  }

  String getName() {
    return name;
  }
  
  int getWidth() {
    return xSize;
  }
  
  int getxPos() {
    return xPos;
  }

  boolean getMouseButtonOver() {
    return mouseOverbutton;
  }
  
  public void drawIngredient() {
    img = sketch.loadImage("images/" + name + ".png");
    sketch.image(img, xPos, yPos, xSize, ySize);
  }

  // Detect if mouse is over the rectangle.
  public void mouseOverButton()  {

    if (sketch.mouseX >= xPos && sketch.mouseX <= xPos+xSize &&
        sketch.mouseY >= yPos && sketch.mouseY <= yPos+ySize) {
      mouseOverbutton = true;
    }
    else {
      mouseOverbutton = false;
    }
  }

  IngredientDisplay set_x_y(int x, int y) {
    return new IngredientDisplay(sketch, name, x, y, xSize, ySize);
  }

  public void drawIngredientHL() {
    img = sketch.loadImage("images/" + name + "hl.png");
    sketch.image(img, xPos, yPos, xSize, ySize);
  }

  public void drawChopped(String ingr) {
    img = sketch.loadImage("images/chopped" + ingr + ".png");
    sketch.image(img, xPos, yPos, xSize, ySize);
  }

  public void drawCooked(String ingr) {
    img = sketch.loadImage("images/" + ingr + ".png");
    sketch.image(img, xPos, yPos, xSize, ySize);
  }

  public void drawWhiskedEggs() {
    img = sketch.loadImage("images/whisked_eggs.png");
    sketch.image(img, xPos, yPos, xSize, ySize);
  }
}

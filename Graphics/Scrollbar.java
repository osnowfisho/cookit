import processing.core.*;

class Scrollbar {

  private float scroll = 0;
  private PApplet sketch;
  private PGraphics messagebox;
  private float textH = 0;
  private int boxw; //200
  private int boxh; //400
  private float tSize; //19
  private int xpos; //0
  private int ypos; //400
  private String s;

  Scrollbar(PApplet sketch, String s, float tSize, int boxw, int boxh, int xpos, int ypos){
    this.sketch = sketch;
    this.s = s;
    this.tSize = tSize;
    this.boxw = boxw;
    this.boxh = boxh;
    this.xpos = xpos;
    this.ypos = ypos;
  }

    // void scrollbarSetup() {
    //   sketch.noStroke();
    //   messagebox = sketch.createGraphics(boxw, boxh);
    // }

  void drawScrollbar() {
    textH = s.length() / 10 * tSize;
    messagebox = sketch.createGraphics(boxw, boxh);
    messagebox.beginDraw();
    messagebox.background(255);
    messagebox.textSize(tSize);
    messagebox.textLeading(tSize + 10);
    messagebox.fill(0);
    messagebox.textAlign(sketch.LEFT, sketch.TOP);
    messagebox.text(s, 0, scroll, messagebox.width, textH);
    messagebox.endDraw();
    sketch.image(messagebox, xpos, ypos);
  }

  void updateScroll(float tmp) {
    scroll -= tmp;
    sketch.constrain(scroll, -textH + 350, 10);//(scroll, min:down, max:top)
  }

    // public void mouseWheel(MouseEvent event) {
    //     scroll -= event.getCount() * 3;
    //     scroll = constrain(scroll, -textH + 350, 10);//(scroll, min:down, max:top)
    // }
}
